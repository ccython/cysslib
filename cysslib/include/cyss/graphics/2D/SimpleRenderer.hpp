#ifndef CYSSLIB_GRAPHICS_2D_SIMPLERENDERER_HPP
#define CYSSLIB_GRAPHICS_2D_SIMPLERENDERER_HPP

#include <cyss/graphics/Texture2D.hpp>

#include <cyss/graphics/2D/Font.hpp>

namespace cyss
{
    class SimpleRenderer
    {
    public:
        SimpleRenderer(unsigned int _width, unsigned int _height);
        ~SimpleRenderer();
        
        void reset();
        void strokeColor(unsigned char _R, unsigned char _G, unsigned char _B);
        void noStroke();
        void fillColor(unsigned char _R, unsigned char _G, unsigned char _B);
        void noFill();
        void drawLine(float _x, float _y, float _x2, float _y2);
        void lineThickness(float _thickness);
        void drawRectangle(float _x, float _y, float _width, float _height);
        void drawEllipse(float _x, float _y, float _width, float _height);
        void drawEllipse(float _x, float _y, float _width, float _height, unsigned int _segmentCount);
        void drawArc(float _x, float _y, float _radius, float _radians);
        void drawArc(float _x, float _y, float _radius, float _radians, unsigned int _segmentCount);
        void drawArc(float _x, float _y, float _radius, float _from, float _to);
        void drawArc(float _x, float _y, float _radius, float _from, float _to, unsigned int _segmentCount);
        void setFont(MonochromeFont* _font);
        void setFontSize(float _size);
        void drawText(float _x, float _y, std::string _text);
        void drawTexture(Texture2D* _texture, float _x, float _y);
        void drawTexture(Texture2D* _texture, float _x, float _y, float _width, float _height);
        void pushMatrix();
        void popMatrix();
        void translateMatrix(float _x, float _y);
        void rotateMatrix(float _radians, float _x, float _y);
    private:
        unsigned char strokeR, strokeB, strokeG;
        unsigned char fillR, fillB, fillG;
        unsigned int m_Width, m_Height;
        MonochromeFont* m_CurrentFont;
        bool m_NoStroke, m_NoFill;
    };
}

#endif