#ifndef CYSSLIB_GRAPHICS_2D_SPRITE_HPP
#define CYSSLIB_GRAPHICS_2D_SPRITE_HPP

#include <string>
#include <unordered_map>

#include <cyss/physics/Rectangle.hpp>
#include <cyss/graphics/Texture2D.hpp>
#include <cyss/graphics/Shader.hpp>
#include <cyss/graphics/VertexArray.hpp>
#include <cyss/graphics/IDrawable.hpp>
#include <cyss/physics/2D/ITransformable2D.hpp>

namespace cyss
{
    //Forward declaration
    class SpriteAnimation;
    
    class Sprite : public IDrawable, public ITransformable2D
    {
    public:
        Sprite();
        Sprite(Texture2D* _texture);
        Sprite(Shader* _shader);
        Sprite(Texture2D* _texture, Shader* _shader);
        ~Sprite();
        
        SpriteAnimation* createAnimation(std::string _name);
        SpriteAnimation* getAnimation(std::string _name);
        void deleteAnimation(std::string _name);
        
        void playAnimation(std::string _name, float _durationMS, bool _looping);
        void setCurrentFrame(Rectangle& _frameRect);
        
        void setSpriteSheet(Texture2D* _texSpriteSheet);
        void loadSpriteSheet(std::string _filename);
        
        //Interface implementation
        virtual void draw() override;
        virtual void setPosition(float _x, float _y) override;
        virtual void setRotation(float _degree) override;
        virtual void setScale(float _scale) override;
    private:
        VertexArray* m_VAO;
        Shader* m_Shader;
        Texture2D* m_SpriteSheet;
        SpriteAnimation* m_CurrentAnimation;
        bool m_IsAnimationLooping;
        float m_AnimationTime;
        std::unordered_map<std::string, SpriteAnimation*> m_Animations;
    };
}

#endif