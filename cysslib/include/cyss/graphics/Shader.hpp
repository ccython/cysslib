#ifndef CYSSLIB_GRAPHICS_SHADER_HPP
#define CYSSLIB_GRAPHICS_SHADER_HPP

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#include <string>
#include <glm/glm.hpp>

#include <cyss/graphics/VertexArray.hpp>

namespace cyss
{
    class Shader
    {
    public:
        Shader();
        ~Shader();
        
        void loadVertex(std::string _filename);
        void loadFragment(std::string _filename);
        
        void use();
        
        void uploadUniform(std::string _uniformname, glm::mat4& _matrix);
        void uploadUniform(std::string _uniformname, glm::vec4& _vector);
        
        bool getIsLinked();
    private:
        void link();
        
        bool m_IsLinked, m_VertexLoaded, m_FragmentLoaded;
        unsigned int m_ProgramID, m_VertexID, m_FragmentID;
    };
}

#endif