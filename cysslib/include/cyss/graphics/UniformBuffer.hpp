#ifndef CYSSLIB_GRAPHICS_UNIFORMBUFFER_HPP
#define CYSSLIB_GRAPHICS_UNIFORMBUFFER_HPP

#include <map>

namespace cyss
{
    class UniformBufferStruct
    {
    public:
        virtual void getBytes() = NULL;
    };
    class UniformBuffer
    {
    public:
        UniformBuffer();
        UniformBuffer(UniformBufferStruct* _data);
        ~UniformBuffer();
    private:
        
    };
}

#endif