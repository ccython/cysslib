#ifndef CYSSLIB_SYS_SINGLETON_HPP
#define CYSSLIB_SYS_SINGLETON_HPP

namespace cyss
{
    template<typename T>
    class Singleton
    {
    public:
        static T* getInstancePtr()
        {
            static T _instance;
            return &_instance;
        }
    };
}

#endif