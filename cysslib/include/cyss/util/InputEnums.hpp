#ifndef CYSSLIB_UTIL_INPUTENUMS_HPP
#define CYSSLIB_UTIL_INPUTENUMS_HPP

namespace cyss
{
    enum KeyboardButton
    {
        KeyUnknown = -1,
        KeySpace = 32,
        KeyApostrophe = 39,
        KeyComma = 44,
        KeyMinus,
        KeyPeriod,
        KeySlash,
        Key0,
        Key1,
        Key2,
        Key3,
        Key4,
        Key5,
        Key6,
        Key7,
        Key8,
        Key9,
        KeySemicolon = 59,
        KeyEqual = 61,
        KeyA = 65,
        KeyB,
        KeyC,
        KeyD,
        KeyE,
        KeyF,
        KeyG,
        KeyH,
        KeyI,
        KeyJ,
        KeyK,
        KeyL,
        KeyM,
        KeyN,
        KeyO,
        KeyP,
        KeyQ,
        KeyR,
        KeyS,
        KeyT,
        KeyU,
        KeyV,
        KeyW,
        KeyX,
        KeyY,
        KeyZ,
        KeyLeftBracket,
        KeyBackslash,
        KeyRightBracket,
        KeyGraveAccent = 96,
        KeyWorld1 = 161,
        KeyWorld2,
        KeyEscape = 256,
        KeyEnter,
        KeyTab,
        KeyBackspace,
        KeyInsert,
        KeyDelete,
        KeyRight,
        KeyLeft,
        KeyDown,
        KeyUp,
        KeyPageUp,
        KeyPageDown,
        KeyHome,
        KeyEnd,
        KeyCapsLock = 280,
        KeyScrollLock,
        KeyNumLock,
        KeyPrintScreen,
        KeyPause,
        KeyF1 = 290,
        KeyF2,
        KeyF3,
        KeyF4,
        KeyF5,
        KeyF6,
        KeyF7,
        KeyF8,
        KeyF9,
        KeyF10,
        KeyF11,
        KeyF12,
        KeyF13,
        KeyF14,
        KeyF15,
        KeyF16,
        KeyF17,
        KeyF18,
        KeyF19,
        KeyF20,
        KeyF21,
        KeyF22,
        KeyF23,
        KeyF24,
        KeyF25,
        KeyNumpad0 = 320,
        KeyNumpad1,
        KeyNumpad2,
        KeyNumpad3,
        KeyNumpad4,
        KeyNumpad5,
        KeyNumpad7,
        KeyNumpad8,
        KeyNumpad9,
        KeyNumpadDecimal = 330,
        KeyNumpadDivide,
        KeyNumpadMultiply,
        KeyNumpadSubtract,
        KeyNumpadAdd,
        KeyNumpadEnter,
        KeyNumpadEqual,
        KeyLeftShift,
        KeyLeftControl,
        KeyLeftAlt,
        KeyLeftSuper,
        KeyRightShift,
        KeyRightControl,
        KeyRightAlt,
        KeyRightSuper,
        KeyMenu,
        KeyLast = KeyMenu,
    };
    
    enum MouseButton
    {
        
    };
    
    enum Joystick
    {
        Joystick0 = 0,
        Joystick1,
        Joystick2,
        Joystick3,
        Joystick4,
        Joystick5,
        Joystick6,
        Joystick7,
        Joystick8,
        Joystick9,
        Joystick10,
        Joystick11,
        Joystick12,
        Joystick13,
        Joystick14,
        Joystick15,
    };
    
    enum JoystickButton
    {
        Btn0 = 0,
        Btn1,
        Btn2,
        Btn3,
        Btn4,
        Btn5,
        Btn6,
        Btn7,
        Btn8,
        Btn9,
        Btn10,
        Btn11,
        Btn12,
        Btn13,
        Btn14,
        Btn15,
    };
    
    enum JoystickAxis
    {
        Axis0 = 0,
        Axix1,
        Axix2,
        Axix3,
        Axix4,
        Axix5,
        Axix6,
        Axix7,
        Axix8,
        Axix9,
        Axix10,
        Axix11,
        Axix12,
        Axix13,
        Axix14,
        Axix15,
    };
}

#endif