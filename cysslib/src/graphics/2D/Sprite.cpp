#include <cyss/graphics/2D/Sprite.hpp>
#include <cyss/graphics/2D/SpriteAnimation.hpp>

using namespace cyss;

Sprite::Sprite()
{
    m_Shader = NULL;
    m_SpriteSheet = NULL;
}

Sprite::Sprite(Shader* _shader)
{
    m_Shader = _shader;
    m_SpriteSheet = NULL;
}

Sprite::Sprite(Texture2D* _texture)
{
    m_Shader = NULL;
    m_SpriteSheet = _texture;
}

Sprite::Sprite(Texture2D* _texture, Shader* _shader)
{
    m_Shader = _shader;
    m_SpriteSheet = _texture;
}

Sprite::~Sprite()
{
    if(m_Shader != NULL) delete m_Shader;
    if(m_SpriteSheet != NULL) delete m_SpriteSheet;
}

SpriteAnimation* Sprite::createAnimation(std::string _name)
{
    if(!m_Animations.count(_name))
    {
        SpriteAnimation* anim = new SpriteAnimation(_name);
        m_Animations[_name] = anim;
        return anim;
    }
    else
    {
        return m_Animations[_name];
    }
}

SpriteAnimation* Sprite::getAnimation(std::string _name)
{
    if(m_Animations.count(_name))
    {
        return m_Animations[_name];
    }
    else
    {
        return NULL;
    }
}

void Sprite::deleteAnimation(std::string _name)
{
    if(m_Animations.count(_name))
    {
        delete m_Animations[_name];
        m_Animations.erase(_name);
    }
}

void Sprite::playAnimation(std::string _name, float _durationMS, bool _looping)
{
}

void Sprite::setCurrentFrame(Rectangle& _frameRect)
{
}

void Sprite::setSpriteSheet(Texture2D* _texSpriteSheet)
{
}

void Sprite::loadSpriteSheet(std::string _filename)
{
}

void Sprite::draw()
{
}

void Sprite::setPosition(float _x, float _y)
{
}

void Sprite::setRotation(float _degree)
{
}

void Sprite::setScale(float _scale)
{
}
