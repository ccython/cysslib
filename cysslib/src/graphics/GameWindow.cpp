#include <cyss/graphics/GameWindow.hpp>

#include <cyss/util/InputManager.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace cyss;

bool GameWindow::s_IsInitialized = false;

GameWindow::GameWindow(std::string _name, unsigned int _width, unsigned int _height)
{
    m_InputManager = NULL;
    if(!s_IsInitialized)
    {
        glfwInit();
        s_IsInitialized = true;
    }
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    GLFWwindow* window_handle = glfwCreateWindow(_width,_height, _name.c_str(), NULL, NULL);
    m_WindowHandle = (void*)window_handle;
    glfwHideWindow(window_handle);
    glfwMakeContextCurrent(window_handle);
    glewInit();
    glDisable(GL_CULL_FACE);
    glClearColor(0.0f,0.0f,0.0f,1.0f);
}
GameWindow::~GameWindow()
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    glfwDestroyWindow(window_handle);
    if(m_InputManager != NULL)
    {
        delete m_InputManager;
    }
}

void* GameWindow::getHandle()
{
    return m_WindowHandle;
}

void GameWindow::show()
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    glfwShowWindow(window_handle);
}

void GameWindow::hide()
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    glfwHideWindow(window_handle);
}

void GameWindow::resize(unsigned int _width, unsigned int _height)
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    glfwSetWindowSize(window_handle, 640, 480);
    glViewport(0,0,_width,_height);
}

void GameWindow::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GameWindow::swapBuffers()
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    glfwSwapBuffers(window_handle);
}

void GameWindow::setClearColor(unsigned char _R, unsigned char _G, unsigned char _B)
{
    glClearColor((float)(_R/(float)255),(float)(_G/(float)255),(float)(_B/(float)255),0.0f);
}

void GameWindow::update()
{
    glfwPollEvents();
}

bool GameWindow::shouldClose()
{
    GLFWwindow* window_handle = (GLFWwindow*)m_WindowHandle;
    bool ret = glfwWindowShouldClose(window_handle);
    return ret;
}

InputManager* GameWindow::getInputManager()
{
    if(m_InputManager == NULL)
    {
        InputManager* ptr = new InputManager(this);
        m_InputManager = ptr;
    }
    return m_InputManager;
}
