#include <cyss/graphics/Shader.hpp>

#include <fstream>
#include <sstream>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace cyss;

Shader::Shader()
{
    m_ProgramID = glCreateProgram();
    m_VertexID = glCreateShader(GL_VERTEX_SHADER);
    m_FragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    m_FragmentLoaded = false;
    m_VertexLoaded = false;
    m_IsLinked = false;
}

Shader::~Shader()
{
    if(!m_IsLinked)
    {
        if(m_VertexLoaded) glDetachShader(m_ProgramID, m_VertexID);
        glDeleteShader(m_VertexID);
        if(m_FragmentLoaded) glDetachShader(m_ProgramID, m_FragmentID);
        glDeleteShader(m_FragmentID);
    }
    glDeleteProgram(m_ProgramID);
}

void Shader::loadVertex(std::string _filename)
{
    if(!m_IsLinked)
    {
        std::ifstream file(_filename);
        std::stringstream ss;
        std::string s;
        ss << file.rdbuf();
        file.close();
        s = ss.str();
        const char* c_source = s.c_str();
        glShaderSource(m_VertexID, 1, &c_source, NULL);
        glCompileShader(m_VertexID);
        glAttachShader(m_ProgramID, m_VertexID);
        m_VertexLoaded = true;
    }
}

void Shader::loadFragment(std::string _filename)
{
    if(!m_IsLinked)
    {
        std::ifstream file(_filename);
        std::stringstream ss;
        std::string s;
        ss << file.rdbuf();
        file.close();
        s = ss.str();
        const char* c_source = s.c_str();
        glShaderSource(m_FragmentID, 1, &c_source, NULL);
        glCompileShader(m_FragmentID);
        glAttachShader(m_ProgramID, m_FragmentID);
        m_FragmentLoaded = true;
    }
}

void Shader::use()
{
    if(m_IsLinked)
    {
        glUseProgram(m_ProgramID);
    }
    else
    {
        glUseProgram(0);
    }
}

void Shader::uploadUniform(std::string _uniformname, glm::mat4& _matrix)
{
    if(m_IsLinked)
    {
        int loc = glGetUniformLocation(m_ProgramID, _uniformname.c_str());
        glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(_matrix));
    }
}
void Shader::uploadUniform(std::string _uniformname, glm::vec4& _vector)
{
    if(m_IsLinked)
    {
        int loc = glGetUniformLocation(m_ProgramID, _uniformname.c_str());
        glUniform4f(loc, _vector.x, _vector.y, _vector.z, _vector.w);
    }
}

bool Shader::getIsLinked()
{
    return m_IsLinked;
}

void Shader::link()
{
    if(m_VertexLoaded && m_FragmentLoaded)
    {
        glLinkProgram(m_ProgramID);
        int is_linked = 0;
        glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &is_linked);
        if(is_linked == GL_TRUE)
        {
            m_IsLinked = true;
            glDetachShader(m_ProgramID, m_VertexID);
            glDeleteShader(m_VertexID);
            glDetachShader(m_ProgramID, m_FragmentID);
            glDeleteShader(m_FragmentID);
        }
    }
}
