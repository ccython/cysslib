#include <cyss/graphics/Texture2D.hpp>

#include <iostream>

#include <GL/glew.h>
#include <Cimg.h>

using namespace cyss;
using namespace cimg_library;

Texture2D::Texture2D(unsigned int _width, unsigned int _height, bool _hasAlpha = false)
{
    m_Width = _width;
    m_Height = _height;
    glGenTextures(1, &m_ID);
    unsigned int id = m_ID;
    GLuint old_id;
    glGetIntegerv(GL_TEXTURE_2D_BINDING, (GLint*)&old_id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    if(_hasAlpha)
    {
        unsigned int* data = new unsigned int[_width*_height*4];
        for(unsigned int i = 0; i < (_width * _height * 4); i += 4)
        {
            data[i] = 255;
            data[i + 1] = 255;
            data[i + 2] = 255;
            data[i + 3] = 255;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        delete data;
    }
    else
    {
        unsigned int* data = new unsigned int[_width*_height*3];
        for(unsigned int i = 0; i < (_width * _height * 3); i+=3)
        {
            data[i] = 255;
            data[i + 1] = 255;
            data[i + 2] = 255;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _width, _height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        delete data;
    }
    glBindTexture(GL_TEXTURE_2D, old_id);
}
Texture2D::~Texture2D()
{
    glDeleteTextures(1, &m_ID);
}

void Texture2D::loadFile(std::string _filename)
{
    CImg<unsigned char> image(_filename.c_str());
    image.resize(m_Width, m_Height);
    GLuint old_id;
    glGetIntegerv(GL_TEXTURE_2D_BINDING, (GLint*)&old_id);
    glBindTexture(GL_TEXTURE_2D, m_ID);
    if(image.spectrum() == 4)
    {
        int size = m_Width * m_Height * 4;
        unsigned char* image_data = image.data();
        unsigned char* real_data = new unsigned char[size];
        
        for(unsigned int i = 0; i != m_Width * m_Height; i++)
        {
            real_data[i * 4 + 0] = image_data[i + 0 * m_Width * m_Height];
            real_data[i * 4 + 1] = image_data[i + 1 * m_Width * m_Height];
            real_data[i * 4 + 2] = image_data[i + 2 * m_Width * m_Height];
            real_data[i * 4 + 3] = image_data[i + 3 * m_Width * m_Height];
        }
        
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, GL_RGBA, GL_UNSIGNED_BYTE, real_data);
        delete real_data;
    }
    else if(image.spectrum() == 3)
    {
        std::cout << "Doesn't have alpha." << std::endl;
        unsigned char* image_data = image.data();
        unsigned char* real_data = new unsigned char[m_Width * m_Height * 3];
        
        for(unsigned int i = 0; i != m_Width * m_Height; i++)
        {
            real_data[i * 3 + 0] = image_data[i + 0 * m_Width * m_Height];
            real_data[i * 3 + 1] = image_data[i + 1 * m_Width * m_Height];
            real_data[i * 3 + 2] = image_data[i + 2 * m_Width * m_Height];
        }
        
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, GL_RGB, GL_UNSIGNED_BYTE, real_data);
        delete real_data;
    }
    glBindTexture(GL_TEXTURE_2D, old_id);
}

void Texture2D::setActive(unsigned int _activeIndex)
{
    glActiveTexture(GL_TEXTURE0 + _activeIndex);
    glBindTexture(GL_TEXTURE_2D, m_ID);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

unsigned int Texture2D::getHeight()
{
    return m_Height;
}

unsigned int Texture2D::getWidth()
{
    return m_Width;
}
