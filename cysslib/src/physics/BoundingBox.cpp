#include <cyss/physics/BoundingBox.hpp>

#include <cmath>

using namespace cyss;

BoundingBox::BoundingBox()
{
    
}
BoundingBox::BoundingBox(vec4f& _center, vec4f& _halfwidth)
{
    m_Center = _center;
    m_Halfwidth = _halfwidth;
}
BoundingBox::~BoundingBox()
{
    
}

bool BoundingBox::intersects(BoundingBox& _other)
{
    if(std::abs(m_Center.X - _other.m_Center.X) > (m_Halfwidth.X + _other.m_Halfwidth.X) ) return false;
    if(std::abs(m_Center.Y - _other.m_Center.Y) > (m_Halfwidth.Y + _other.m_Halfwidth.Y) ) return false;
    if(std::abs(m_Center.Z - _other.m_Center.Z) > (m_Halfwidth.Z + _other.m_Halfwidth.Z) ) return false;
    // We have an overlap
    return true;
}