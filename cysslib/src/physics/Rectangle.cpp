#include <cyss/physics/Rectangle.hpp>

using namespace cyss;

Rectangle::Rectangle(float _x, float _y, float _width, float _height)
{
    X = _x;
    Y = _y;
    Width = _width;
    Height = _height;
}

Rectangle::~Rectangle()
{
    
}

bool Rectangle::intersects(Rectangle& _rect)
{
    return (
        (X < _rect.X + _rect.Width)&&
        (X + Width > _rect.X)&&
        (Y < _rect.Y + _rect.Height)&&
        (Y + Height > _rect.Y)
    );
}

bool Rectangle::intersects(glm::vec2& _vec)
{
    return (
        (X <= _vec.x)&&
        (X + Width >= _vec.x)&&
        (Y <= _vec.y)&&
        (Y + Height >= _vec.y)
    );
}
