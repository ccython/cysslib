#include <cyss/sys/Component.hpp>

#include <cyss/sys/IDGenerator.hpp>

using namespace cyss;
using namespace cyss::ecs;

Component::Component(std::string _componentName)
{
    m_UniqueID = IDGenerator::getInstancePtr()->getNextID();
    m_ComponentName = _componentName;
}
Component::~Component()
{
    
}

std::string Component::getComponentName()
{
    return m_ComponentName;
}