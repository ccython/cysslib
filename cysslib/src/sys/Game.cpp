#include <cyss/sys/Game.hpp>

#include <chrono>
#include <thread>

using namespace cyss;

Game::Game()
{
    m_IsRunning = false;
}
Game::~Game()
{
    
}

void Game::run()
{
    initialize();
    loadContent();
    m_IsRunning = true;
    while(m_IsRunning)
    {
        update();
        draw();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    unloadContent();
}

void Game::shutdown()
{
    m_IsRunning = false;
}