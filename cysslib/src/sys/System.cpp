#include <cyss/sys/System.hpp>
#include <cyss/sys/IDGenerator.hpp>

using namespace cyss;
using namespace cyss::ecs;

System::System()
{
    m_Name = "";
    m_UniqueID = IDGenerator::getInstancePtr()->getNextID();
    m_IsLoaded = false;
    m_Entities = NULL;
}
System::System(std::string _name)
{
    m_Name = _name;
    m_UniqueID = IDGenerator::getInstancePtr()->getID(_name);
    m_IsLoaded = false;
    m_Entities = NULL;
}
System::~System()
{
    
}
void System::_update(std::vector<std::shared_ptr<Entity>>* _entites, float _deltaMS)
{
    if(!m_IsLoaded)
    {
        _load();
    }
    m_Entities = _entites;
    update(_deltaMS);
}
void System::_update(std::vector<std::shared_ptr<Entity>>* _entites, float _deltaMS, bool _shouldUnload)
{
    if(_shouldUnload)
    {
        _unload();
    }
    m_Entities = _entites;
    update(_deltaMS);
}
void System::_load()
{
    load();
    m_IsLoaded = true;
}
void System::_unload()
{
    unload();
    m_IsLoaded = false;
}