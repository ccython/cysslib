#include <cyss/sys/World.hpp>

using namespace cyss;
using namespace cyss::ecs;

World::World()
{
    
}
World::~World()
{
    for(auto entityPtr : m_Entities)
    {
        entityPtr.reset();
    }
    for(auto systemPtr : m_Systems)
    {
        systemPtr.reset();
    }
}

Entity* World::newEntity()
{
    Entity* ptr = new Entity();
    std::shared_ptr<Entity> entity(ptr);
    m_Entities.push_back(entity);
    return ptr;
}
Entity* World::newEntity(std::string _entityName)
{
    Entity* ptr = new Entity(_entityName);
    std::shared_ptr<Entity> entity(ptr);
    m_Entities.push_back(entity);
    return ptr;
}
Entity* World::newEntityFromPrototype(std::string _prototypeName)
{
    return NULL;
}
Entity* World::newEntityFromPrototype(std::string _prototypeName, std::string _entityName)
{
    return NULL;
}
void World::addEntityPrototype(std::string _prototypeName, size_t _entityID)
{
    
}
void World::deleteEntity(size_t _entityID)
{
    
}

void World::addSystem(System* _system)
{
    std::shared_ptr<System> ptr(_system);
    m_Systems.push_back(ptr);
}
void World::deleteSystem(size_t _systemID)
{
    
}

void World::update(float _deltaMS)
{
    for(auto system : m_Systems)
    {
        system->_update(&(m_Entities), _deltaMS);
    }
}

