#include <cyss/util/Logger.hpp>
#include <ctime>
#include <sstream>
#include <iostream>
#include <thread>
#include <chrono>

using namespace cyss;

Logger::Logger()
{
    
}
Logger::~Logger()
{
    
}

void Logger::consoleLog(std::string _from, std::string _message)
{
    m_Mutex.lock();
    time_t t = time(0);
    tm* now = localtime(&t);
    std::stringstream ss;
    ss << "[" << now->tm_mday << "." << now->tm_mon+1 << "." << now->tm_year + 1900 << "]";
    ss << "[" << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << "]";
    ss << "<<" << _from << ">> : " << _message;  
    std::cout << ss.str() << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    m_Mutex.unlock();
}